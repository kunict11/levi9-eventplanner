# Event Planner

Event Planner is a calendar web app for organizing your schedule.

The app was developed using React with the Next.js framework on frontend and MongoDB, Express.js and Node.js on backend.

## Database

To populate the database run:

```bash
mongoimport --db event_planner --collection users --file users.json --jsonArray
```

from the project folder.

## Build and run

After cloning the repository and navigating to the project folder, to start the server use:

```bash
cd ./server
npm install
npm run start
```

To build and run the client, also from the project folder, use:

```bash
cd ./client
npm install
npm run dev
```

Alternatively, if you have Docker and docker-compose installed:

```bash
docker-compose up --build -d
```
