const serverHost = process.env.REACT_APP_HOST || 'localhost';
export const userUrl = `http://${serverHost}:8080/api/users`;
export const eventUrl = `http://${serverHost}:8080/api/events`;
