const getFirstDayOfMonth = (currentDate) => {
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);
};

const getLastDayOfMonth = (currentDate) => {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
};
const getDayIndex = (date) => {
    return date.getDay() === 0 ? 7 : date.getDay();
};

export const getCurrentMonthName = (currentDate) => {
    return currentDate.toLocaleDateString('en-uk', { month: 'long' });
};

export const getWeeksInMonth = (currentDate) => {
    const firstDayOfMonth = getFirstDayOfMonth(currentDate);
    const lastDayOfMonth = getLastDayOfMonth(currentDate);

    return Math.ceil(
        (getDayIndex(firstDayOfMonth) + lastDayOfMonth.getDate()) / 7.0
    );
};

export const getDateString = (date) => {
    return date.toISOString().split('T')[0];
};

export const isValidDay = (dayOfWeek, week, dayOfMonth, currentDate) => {
    const firstDayOfMonth = getFirstDayOfMonth(currentDate);
    const lastDayOfMonth = getLastDayOfMonth(currentDate);
    if (
        (week === 0 && dayOfWeek < getDayIndex(firstDayOfMonth)) ||
        dayOfMonth > lastDayOfMonth.getDate()
    )
        return false;
    return true;
};

export const nextMonth = (currentDate) => {
    let month = currentDate.getMonth();
    let year = currentDate.getFullYear();
    if (month + 1 > 11) {
        month = 0;
        year += 1;
    } else month += 1;
    return new Date(year, month);
};

export const previousMonth = (currentDate) => {
    let month = currentDate.getMonth();
    let year = currentDate.getFullYear();
    if (month - 1 < 0) {
        month = 11;
        year -= 1;
    } else month -= 1;
    return new Date(year, month);
};
