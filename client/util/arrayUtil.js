export const groupBy = (array, property) => {
    const grouped = array.reduce((acc, el) => {
        if (!acc[el[property]]) {
            acc[el[property]] = [];
        }

        acc[el[property]].push(el);
        return acc;
    }, {});
    return grouped;
};
