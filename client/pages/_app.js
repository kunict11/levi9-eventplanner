import { ThemeProvider, createTheme, CssBaseline } from '@mui/material';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
    const darkTheme = createTheme({
        palette: {
            mode: 'dark',
            primary: { main: '#ffb74d' },
            background: {
                default: '#202020',
                paper: '#424242',
            },
        },
    });
    return (
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <Component {...pageProps} />
        </ThemeProvider>
    );
}

export default MyApp;
