import { Alert, Container } from '@mui/material';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import Modal from '../../src/components/Modal';
import { Box } from '@mui/system';
import Link from 'next/link';
import EventCard from '../../src/components/EventCard';
import { eventUrl } from '../../util/url';

const EventInfo = () => {
    const router = useRouter();
    const { id } = router.query;
    const [event, setEvent] = useState(null);

    const [deleteResponse, setDeleteResponse] = useState(null);

    useEffect(() => {
        if (id) {
            fetch(`${eventUrl}/${id}`)
                .then((res) => res.json())
                .then((json) => {
                    setEvent(json);
                });
        }
    }, [id]);

    const deleteEvent = () => {
        fetch(`${eventUrl}/${id}`, { method: 'DELETE' })
            .then((res) => {
                if (res.ok) {
                    return res.json();
                } else {
                    return res.json().then((err) => Promise.reject(err));
                }
            })
            .then((json) => {
                setDeleteResponse({
                    message: json.message,
                    ok: true,
                });
            })
            .catch((err) => {
                const msg = `Error deleting event (${err.status}): ${err.message}`;
                setDeleteResponse({
                    message: msg,
                    ok: false,
                });
            });
    };

    return (
        <Container style={{ marginTop: '2rem' }}>
            {deleteResponse ? (
                deleteResponse.ok ? (
                    <Modal title='Delete event'>
                        <Box style={{ color: '#C4C4C4' }}>
                            {deleteResponse.message}
                        </Box>
                        <Box
                            style={{
                                marginTop: '1.5rem',
                                alignSelf: 'flex-end',
                            }}
                        >
                            <Link href={'/'}>
                                <Button variant='contained'>Ok</Button>
                            </Link>
                        </Box>
                    </Modal>
                ) : (
                    <Alert variant='filled' severity='error'>
                        {deleteResponse.message}
                    </Alert>
                )
            ) : (
                ''
            )}
            {event ? <EventCard event={event} /> : ''}
            <Button
                variant='outlined'
                startIcon={<DeleteIcon />}
                className='delete-button'
                onClick={deleteEvent}
            >
                Delete
            </Button>
        </Container>
    );
};

export default EventInfo;
