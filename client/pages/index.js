import { Alert, Container, Stack } from '@mui/material';
import Calendar from '../src/components/Calendar';
import styles from '../styles/Home.module.css';
import { useState, useEffect } from 'react';
import { eventUrl } from '../util/url';
import {
    getCurrentMonthName,
    nextMonth,
    previousMonth,
} from '../util/dateUtil';

export default function Home() {
    const [events, setEvents] = useState([]);
    const [currentDate, setCurrentDate] = useState(new Date());

    const [postResponseError, setPostResponseError] = useState(false);
    const [counter, setCounter] = useState(0);

    useEffect(() => {
        fetch(eventUrl)
            .then((res) => res.json())
            .then((eventsJson) => setEvents(eventsJson));
    }, [counter]);

    const getNextMonth = () => {
        setCurrentDate(nextMonth(currentDate));
    };

    const getPreviousMonth = () => {
        setCurrentDate(previousMonth(currentDate));
    };
    const triggerUpdate = () => {
        setCounter(counter + 1);
    };
    return (
        <Container>
            {postResponseError ? (
                <Alert variant='filled' severity='error'>
                    An error occured while adding a new event
                </Alert>
            ) : (
                ''
            )}
            <Stack
                spacing={1}
                direction={'row'}
                className={styles['title-wrap']}
            >
                <button
                    onClick={getPreviousMonth}
                    className={styles['btn-switch']}
                >
                    {'<<'}
                </button>
                <h1 className={styles.title}>
                    {`${getCurrentMonthName(
                        currentDate
                    )} ${currentDate.getFullYear()}`}
                </h1>
                <button onClick={getNextMonth} className={styles['btn-switch']}>
                    {'>>'}
                </button>
            </Stack>
            <Calendar
                currentDate={currentDate}
                events={events}
                setResponseError={setPostResponseError}
                triggerFetch={triggerUpdate}
            />
        </Container>
    );
}
