import { Box, Grid, Stack } from '@mui/material';
import { useState } from 'react';
import Event from '../Event';
import EventForm from '../EventForm';
import style from './EventDay.module.css';
import Modal from '../Modal';

const EventDay = ({
    date,
    scheduledEvents,
    setResponseError,
    triggerFetch,
}) => {
    const day = new Date(date).getDate();

    const [formOpen, setFormOpen] = useState(false);

    const openEventForm = () => {
        setFormOpen(true);
    };
    const cancelInput = () => {
        setFormOpen(false);
    };

    return (
        <div onDoubleClick={openEventForm} className={style['event-wrap']}>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    <Box>{day}</Box>
                </Grid>
                <Grid item xs={12} className={style.info}>
                    <Stack spacing={1}>
                        {scheduledEvents.map((event) => (
                            <Event key={event._id} event={event} />
                        ))}
                    </Stack>
                </Grid>
            </Grid>
            {formOpen ? (
                <Modal title='Schedule an event'>
                    <EventForm
                        date={date}
                        onCancel={cancelInput}
                        setResponseError={setResponseError}
                        triggerFetch={triggerFetch}
                    />
                </Modal>
            ) : (
                ''
            )}
        </div>
    );
};

export default EventDay;
