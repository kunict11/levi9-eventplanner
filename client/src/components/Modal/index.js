import { Box } from '@mui/system';
import style from './Modal.module.css';

const Modal = ({ title, children }) => {
    return (
        <Box className={style.overlay}>
            <Box className={style.modal}>
                <h2>{title}</h2>
                {children}
            </Box>
        </Box>
    );
};

export default Modal;
