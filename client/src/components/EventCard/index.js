import { Fragment } from 'react';
import Typography from '@mui/material/Typography';
import { Card, CardContent, Box } from '@mui/material';
import AttendeesList from '../AttendeesList';

const EventCard = ({ event }) => {
    const eventDate = new Date(event.date);
    return (
        <Card>
            <Fragment>
                <CardContent>
                    <Typography variant='h5' component='div'>
                        {event.name}
                    </Typography>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color='text.secondary'
                        gutterBottom
                    >
                        {eventDate.toLocaleDateString('en-uk')} at {event.time}
                    </Typography>
                    <Typography sx={{ mb: 1.5 }} color='text.secondary'>
                        {event.description}
                    </Typography>
                    {event.attendees.length > 0 ? (
                        <Box>
                            <Typography variant='body2'>Attendees:</Typography>
                            <AttendeesList attendees={event.attendees} />
                        </Box>
                    ) : (
                        ''
                    )}
                </CardContent>
            </Fragment>
        </Card>
    );
};

export default EventCard;
