import { Chip, Stack } from '@mui/material';

const AttendeesList = ({ attendees }) => {
    return (
        <Stack direction='row' spacing={1} mt={'0.5rem'}>
            {attendees.map((user) => (
                <Chip label={user.name} />
            ))}
        </Stack>
    );
};

export default AttendeesList;
