import { Button, FormControl, TextField } from '@mui/material';
import { Box } from '@mui/system';
import style from './EventForm.module.css';
import { useState, useEffect } from 'react';
import { userUrl, eventUrl } from '../../../util/url';
import SelectList from '../SelectList';

const EventForm = ({ date, onCancel, setResponseError, triggerFetch }) => {
    const [users, setUsers] = useState([]);
    const [selectedUsers, setSelectedUsers] = useState([]);

    const [eventName, setEventName] = useState('');
    const [eventDesc, setEventDesc] = useState('');
    const [eventTime, setEventTime] = useState('');

    const [nameError, setNameError] = useState({ ok: true, message: '' });
    const [descError, setDescError] = useState({ ok: true, message: '' });
    const [timeError, setTimeError] = useState({ ok: true, message: '' });

    useEffect(() => {
        fetch(userUrl)
            .then((res) => res.json())
            .then((json) => setUsers(json));
    }, []);

    const nameChanged = (e) => {
        setEventName(e.target.value);
    };

    const descChanged = (e) => {
        setEventDesc(e.target.value);
    };

    const validateTimeString = (time) => {
        if (!time) {
            setTimeError({ ok: false, message: 'Time is required' });
            return false;
        }
        let re = new RegExp(
            /([01][0-9]|2[0-4]):([0-5][0-9])\s?-\s?([01][0-9]|2[0-4]):([0-5][0-9])/
        );
        let matchGroups = time.match(re);
        if (!matchGroups) {
            setTimeError({
                ok: false,
                message:
                    'Invalid time format. The correct format is hh:mm - hh:mm.',
            });
            return false;
        }
        const h1 = Number.parseInt(matchGroups[1]);
        const m1 = Number.parseInt(matchGroups[2]);
        const h2 = Number.parseInt(matchGroups[3]);
        const m2 = Number.parseInt(matchGroups[4]);

        if (h1 > h2 || (h1 === h2 && m1 > m2)) {
            setTimeError({ ok: false, message: 'Invalid time span.' });
            return false;
        }
        setTimeError({ ok: true, message: '' });
        return true;
    };

    const timeChanged = (e) => {
        const time = e.target.value;
        validateTimeString(time);
        setEventTime(time);
    };

    const validateName = (name) => {
        if (!name) {
            setNameError({ ok: false, message: 'Name is required' });
            return false;
        }
        setNameError({ ok: true, message: '' });
        return true;
    };

    const validateDesc = (desc) => {
        if (!desc) {
            setDescError({ ok: false, message: 'Description is required' });
            return false;
        }
        setDescError({ ok: true, message: '' });
        return true;
    };

    const validateForm = (e) => {
        e.preventDefault();
        if (
            !validateName(eventName) ||
            !validateDesc(eventDesc) ||
            !validateTimeString(eventTime)
        ) {
            return;
        }
        const startTime = eventTime.split('-')[0].trim();
        const dateStr = date + ' ' + startTime + 'Z';
        const eventDate = new Date(dateStr);
        addEvent(eventName, eventDesc, eventDate, eventTime, selectedUsers);
    };

    const addEvent = (name, desc, date, time, attendees) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name: name,
                description: desc,
                date: date,
                time: time,
                attendees: attendees,
            }),
        };
        fetch(eventUrl, requestOptions)
            .then((res) => {
                if (res.ok) {
                    onCancel();
                    triggerFetch();
                    return;
                } else {
                    return Promise.reject();
                }
            })
            .catch(() => {
                onCancel();
                setResponseError(true);
            });
    };

    const onSelectChange = (e) => {
        const selectedId = e.target.value[e.target.value.length - 1];
        if (selectedUsers.some((user) => selectedId === user._id)) return;
        const user = users.find((user) => user._id === selectedId);
        setSelectedUsers([...selectedUsers, user]);
    };

    return (
        <div className={style['form-wrap']}>
            <form
                className={style['form-wrap']}
                onSubmit={(e) => validateForm(e)}
            >
                <FormControl className={style.control}>
                    <TextField
                        className={style.input}
                        label='Event Name'
                        onChange={(e) => nameChanged(e)}
                        error={!nameError.ok}
                        helperText={nameError.message}
                    />
                </FormControl>
                <FormControl className={style.control}>
                    <TextField
                        className={style.input}
                        multiline
                        label='Description'
                        rows={4}
                        onChange={(e) => descChanged(e)}
                        error={!descError.ok}
                        helperText={descError.message}
                    />
                </FormControl>
                <FormControl className={style.control}>
                    <TextField
                        className={style.input}
                        label='Time'
                        placeholder='ex. 09:00 - 10:30'
                        onChange={(e) => timeChanged(e)}
                        error={!timeError.ok}
                        helperText={timeError.message}
                    />
                </FormControl>
                <FormControl className={style.control}>
                    <SelectList
                        options={users}
                        values={selectedUsers.map((user) => user.name)}
                        onChange={onSelectChange}
                        label={'Attendees'}
                        className={style.input}
                    />
                </FormControl>
                <Box className={`${style.input} ${style['btn-wrap']}`}>
                    <Button
                        className={`${style.btn} ${style['btn-cancel']}`}
                        variant='outlined'
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                    <Button
                        className={`${style.btn}`}
                        variant='contained'
                        type='submit'
                    >
                        Add
                    </Button>
                </Box>
            </form>
        </div>
    );
};

export default EventForm;
