import {
    Chip,
    InputLabel,
    MenuItem,
    OutlinedInput,
    Select,
} from '@mui/material';
import { Box } from '@mui/system';
import styles from './SelectList.module.css';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const SelectList = ({ options, values, onChange, label, className }) => {
    return (
        <>
            <InputLabel className={className} htmlFor='multiple-select-lbl'>
                {label}:
            </InputLabel>
            <Select
                labelId='multiple-select-lbl'
                id='multiple-select'
                className={className}
                multiple
                value={values}
                onChange={(e) => onChange(e)}
                input={
                    <OutlinedInput id='select-multiple-chip' label={label} />
                }
                renderValue={(selected) => (
                    <Box className={styles['value-wrap']}>
                        {selected.map((value) => (
                            <Chip key={value} label={value} />
                        ))}
                    </Box>
                )}
                MenuProps={MenuProps}
            >
                {options.map((user) => (
                    <MenuItem key={user._id} value={user._id}>
                        {user.name}
                    </MenuItem>
                ))}
            </Select>
        </>
    );
};

export default SelectList;
