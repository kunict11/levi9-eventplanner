import { Container } from '@mui/material';
import { getColor } from '../../../util/color';
import style from './Event.module.css';
import Link from 'next/link';

const Event = ({ event }) => {
    return (
        <Link href={`/event/${event._id}`}>
            <Container
                style={{ backgroundColor: `${getColor()}`, color: 'black' }}
                className={style.link}
            >
                <a>
                    <h4> {event.name}</h4>
                    <span>{event.time}</span>
                </a>
            </Container>
        </Link>
    );
};

export default Event;
