import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import EventDay from '../EventDay';
import style from './Calendar.module.css';
import { groupBy } from '../../../util/arrayUtil';
import {
    getDateString,
    isValidDay,
    getWeeksInMonth,
} from '../../../util/dateUtil';

const Calendar = ({ currentDate, events, setResponseError, triggerFetch }) => {
    const groupEventsByDate = (events) => {
        events.forEach(
            (event) => (event.date = getDateString(new Date(event.date)))
        );
        return groupBy(events, 'date');
    };

    const groupedEvents = groupEventsByDate(events);

    const setDays = () => {
        let tbody = [];
        const weeksInMonth = getWeeksInMonth(currentDate);

        let day = 1;
        let cellkey = 100;

        for (let i = 0; i < weeksInMonth; i++) {
            let tdata = [];
            for (let j = 1; j <= 7; j++) {
                let key = day;
                let component = '';
                if (isValidDay(j, i, day, currentDate)) {
                    const eventDate = new Date(
                        currentDate.getFullYear(),
                        currentDate.getMonth(),
                        day + 1
                    );
                    const eventString = getDateString(eventDate);
                    component = (
                        <EventDay
                            date={eventString}
                            scheduledEvents={
                                groupedEvents[eventString]
                                    ? groupedEvents[eventString]
                                    : []
                            }
                            setResponseError={setResponseError}
                            triggerFetch={triggerFetch}
                        ></EventDay>
                    );
                    day += 1;
                } else {
                    key = cellkey;
                    cellkey += 1;
                }
                tdata.push(
                    <TableCell
                        className={style.cell}
                        align='center'
                        key={key}
                        className={style.cell}
                    >
                        {component}
                    </TableCell>
                );
            }
            tbody.push(<TableRow key={i}>{tdata}</TableRow>);
        }

        return tbody;
    };
    return (
        <TableContainer>
            <Table style={{ tableLayout: 'fixed' }}>
                <TableHead>
                    <TableRow>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                        <th>Sunday</th>
                    </TableRow>
                </TableHead>
                <TableBody>{setDays()}</TableBody>
            </Table>
        </TableContainer>
    );
};

export default Calendar;
