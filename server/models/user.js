const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.getAllUsers = async () => {
    return await User.find({}).exec();
};

module.exports.addUser = async (name, email) => {
    const user = new User();

    user._id = new mongoose.Types.ObjectId();
    user.name = name;
    user.email = email;

    return await user.save();
};