const mongoose = require('mongoose');

const EventSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    attendees: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

const Event = module.exports = mongoose.model('Event', EventSchema);

module.exports.getAllEvents = async () => {
    return await Event.find({}).sort({date: 1}).populate('attendees').exec();
};

module.exports.getEventById = async (id) => {
    return await Event.findById(id).populate('attendees').exec();
};

module.exports.addNewEvent = async (name, description, date, time, attendees) => {
    const event = new Event();

    event._id = new mongoose.Types.ObjectId();
    event.name = name;
    event.description = description;
    event.date = new Date(date);
    event.time = time;
    event.attendees = attendees;

    return await event.save();
};

module.exports.deleteEvent = async (id) => {
    return await Event.deleteOne({ _id: id }).exec();
};