const Event = require('../models/event');

module.exports.getAllEvents = async (req, res, next) => {
    try {
        const events = await Event.getAllEvents();

        return res.status(200).json(events);
    } catch(err) {
        next(err);
    }
};

module.exports.getEventById = async (req, res, next) => {
    const eventId = req.params.id;

    try {
        const event = await Event.getEventById(eventId);

        if (!event) {
            const error = new Error(`Event with id ${eventId} doesn't exist.`);
            error.status = 404;
            throw error
        } 
        res.status(200).json(event);
    } catch(err) {
        next(err);
    }
};

module.exports.addNewEvent = async (req, res, next) => {
    const name = req.body.name;
    const description = req.body.description;
    const date = req.body.date;
    const time = req.body.time;
    const attendees = req.body.attendees;

    try {
        if (name === '' || description === '' || date === '' || time === '') {
            const error = new Error('Invalid request: Some fields might be missing.');
            error.status = 400;

            throw error;
        }
        const event = await Event.addNewEvent(name, description, date, time, attendees);

        res.status(201).json(event);
    } catch(err) {
        next(err);
    }
}

module.exports.deleteEventById = async (req, res, next) => {
    const eventId = req.params.id;

    try {
        const event = await Event.getEventById(eventId);

        if(!event) {
            const error = new Error(`Event with given id doesn't exist.`);
            error.status = 404;
            throw error
        }

        await Event.deleteEvent(eventId);

        res.status(200).json({ message: `Successfully deleted event: '${event.name}'.` });

    } catch(err) {
        next(err);
    }
}