const User = require('../models/user');

module.exports.getAllUsers = async (req, res, next) => {
    try {
        const users = await User.getAllUsers();

        res.status(200).json(users);
    } catch(err) {
        next(err);
    }
}

module.exports.addUser = async (req, res, next) => {
    const name = req.body.name;
    const email = req.body.email;

    try {
        if (name === '' || email === '') {
            const error = new Error('Some parameters might be missing.');
            error.status = 400;

            throw error;
        }

        const user = await User.addUser(name, email);

        res.status(201).json(user);
    } catch(err) {
        next(err);
    }
};