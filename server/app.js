const express = require('express');
const mongoose = require('mongoose');

const dbHost = process.env.DB_HOST || 'localhost'

const dbString = `mongodb://${dbHost}:27017/event_planner`;
mongoose.connect(dbString, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.connection.on('connected', () => {
    console.log(`Connected to database ${dbString}`);
});
mongoose.connection.on('error', (error) => {
    console.log('Error connecting to db: ', error);
  });  

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE');
        return res.status(200).json();
    }

    next();
});

const usersAPI = require('./routes/userRoutes');
const eventsAPI = require('./routes/eventRoutes');

app.use('/api/users', usersAPI);
app.use('/api/events', eventsAPI);

app.use((req, res, next) => {
    const error = new Error('This request is not supported.');
    error.status = 405;

    next(error);
});

app.use((err, req, res, next) => {
    const status = err.status || 500;

    res.status(status).json({
        message: err.message,
        status: status,
        stack: err.stack
    });
});

module.exports = app;