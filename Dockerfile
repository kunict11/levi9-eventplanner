FROM mongo:4.4.11

COPY users.json /users.json

CMD mongoimport --uri mongodb://mongodb:27017/event_planner --collection users --file users.json --jsonArray